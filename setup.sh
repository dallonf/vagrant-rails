#! /bin/bash

if ! [ -a $HOME/.provisioned ]
  then
    sudo apt-get update
    sudo apt-get install build-essential zlib1g-dev curl git-core sqlite3 libsqlite3-dev -y

    # Install Node.js
    sudo apt-get install python-software-properties -y
    sudo add-apt-repository ppa:chris-lea/node.js
    sudo apt-get update
    sudo apt-get install nodejs npm -y

    # Install rbenv
    git clone git://github.com/sstephenson/rbenv.git ~/.rbenv
    git clone git://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build

    export PATH="$HOME/.rbenv/bin:$PATH"
    eval "$(rbenv init -)"

    echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.profile
    echo 'eval "$(rbenv init -)"' >> ~/.profile

    # Install ruby
    rbenv install 1.9.3-p392
    rbenv global 1.9.3-p392
    gem install bundle
    rbenv rehash

    gem install rubygems-bundler
    gem install rails
    gem regenerate_binstubs

    # Set up samba
    sudo apt-get install samba -y
    mkdir ~/devroot
    echo "[devroot]
path = /home/vagrant/devroot
read only = no
writable = yes" | sudo tee -a /etc/samba/smb.conf
    (echo "vagrant"; echo "vagrant") | sudo smbpasswd -as vagrant
    
    sudo restart smbd
    sudo restart nmbd

    # Set up PhantomJS
    cd /usr/local/share
    sudo curl -o phantomjs.tar.bz2 http://phantomjs.googlecode.com/files/phantomjs-1.8.1-linux-i686.tar.bz2
    sudo tar xvjf phantomjs.tar.bz2
    sudo rm phantomjs.tar.bz2
    sudo mv phantomjs-1.8.1-linux-i686/ phantomjs
    sudo ln -s /usr/local/share/phantomjs/bin/phantomjs /usr/local/bin/phantomjs
    sudo apt-get install libfreetype6 libfreetype6-dev libfontconfig -y

    sudo apt-get install libxslt-dev libxml2-dev -y

    touch $HOME/.provisioned
fi